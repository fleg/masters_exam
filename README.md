Master Exam
===========

You probably are interested in automatically generated
PDF, get it [here](http://dev.mkurzeja.pl/exam.pdf) (thanks to misi3kk)

Compilation
-----------

    cmake ./
    make

Output pdf should be present in the `out` directory.
