JPEG, shorthand for Joint Photographic Experts Group, is the name of a standard
used for compression of still images. The standard itself specifies two kinds
of compression algorithms : lossy (based on Discrete Cosine Transform) and
lossless (based on wavelet transform). However, the lossless kind of
compression is rarely used in practice and therefore usually not implemented by
the software supporting JPEG.

Firstly, the input image is split into 8 $\times$ 8 blocks. If the width or the
height of the image is not a multiple of 8, then the encoder applies additional
padding, which must be discarded when decoding the image. The contents of the
padding should be the same as the content of the neighbouring blocks.

After loading the data into the 8 $\times$ 8 blocks, every one of the values in
the blocks is level shifted by subtracting $2^{P-1}$ from the input value. The
parameter $P$ is the precision of the encoding. In the default algorithm, it
always equals 8, although the specification allows 12--bit samples as well.
Therefore, the shift level will be 128 and 2048, respectively.

The level shifted input values are then subject to transformation by the
Discrete Cosine Transform. Mathematically, the transformation is defined by
this equation :

\[ S_{vu} = \frac{1}{4} C_u C_v 
    \sum_{x=0}^7{
    \sum_{y=0}^7{
    s_{yx}
    \cos{\frac{\left( 2x+1 \right) u \pi}{16}}
    \cos{\frac{\left( 2y+1 \right) v \pi}{16}}
    }
    }
\]

\[ C_u = C_v = \left\{
    \begin{array}{l l}
        1 / \sqrt{2} & \quad \text{if $u$ or $v$ = 0} \\
        1 & \quad \text{otherwise}
    \end{array}
    \right.
\]

The final step of the encoding process is quantization. It is performed by
dividing the given coefficient by the corresponding value found in the
quantization table :

\[ S_{qvu} = \text{round} \left( \frac{S_{vu}}{Q_{vu}} \right) \]

$S_{qvu}$ is the final quantized value of the coefficient, while $S_{vu}$ is
the value after applying DCT. $Q_{vu}$ is the value from the quantization table.

The actual encoding applied by the JPEG algorithm ends at this point. However,
before writing the output of the algorithm to the final file, additional
encoding is applied. Firstly, it is important to note the difference between AC
and DC coefficients. AC coefficients are stored within each encoded block by
using the zig--zag sequence, while DC coefficients are stored by storing the
difference of the actual values between the current block and the previous
block. The value of the difference for the first block is set to zero. Data
prepared in this way is then compressed by using Huffman coding.
