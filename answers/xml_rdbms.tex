% everything that's here applies to MsSQL, since that's what Nikodem covered
% during Advanced Databases. it's a lot, but I doubt if they're going to be
% that picky.
XML extensions available in various relational database systems vary between
the systems' vendors. The most common extension is the ability to store XML
documents inside the RDBMS, while performing their validation and ensuring that
they conform to a pre--set schema. Exporting the contents of the database to
XML is also similarly common. The extensions described here are supported by
the Microsoft SQL Server.

XML can, of course, always be stored inside the database as a \texttt{BLOB} or
\texttt{TEXT} type, depending on the name of the type. However, this kind of
storage does not have any advantages, and its disadvantages are very severe :
the processing of such fields is much harder, because functions operating on
them must ensure correctness of the underlying XML. This usually requires an
external XML parser, which performs serialization and deserialization of the
data to/from XML. Of course, if storage of the XML data is the only thing that
is needed, then XML extensions are not needed at all.

The most convenient extension is the ability to store XML in a column with a
type that's dedicated to storing XML. This column can be either \textit{typed}
or \textit{untyped}, depending on how much information the database server has
about the underlying XML :
\begin{itemize}
    \item \textit{Typed} column has an associated schema with it. The server
    can place indices on the elements of the documents, perform validity checks,
    and arithmetic operations. Typed columns should be used when the schema for
    the XML that's stored in an XML column in known and when the XML that is
    going to be stored inside the database is guaranteed to be well--formed.
    
    \item \textit{Untyped} columns are pretty much identical to using
    \texttt{BLOB} or \texttt{TEXT} columns. The server does not check the
    validity of the data and cannot access the actual data inside the XML.
\end{itemize}

Outputting XML documents directly from the RDBMS can be done by using extra
\texttt{FOR XML} option with the SQL \texttt{SELECT} clause. In order to
illustrate the results of these commands, let's consider the following table,
whose name is \texttt{Items} :
\begin{table}[H]
    \centering
    \begin{tabular}{ | c | c | c | } \hline
        item\_id & name & price \\ \hline
        1 & Sprite & 4.50 \\ \hline
        2 & Coca--Cola & 4.25 \\ \hline
        3 & Diamond Pony & 9000 \\ \hline
    \end{tabular}
    \caption{Example table to illustrate XML output from RDBMS}
\end{table}

The output is going to be dependent on the value of the option passed to the
\texttt{FOR XML} clause. Allowed options include :
\begin{itemize}
    \item \texttt{AUTO}. In this option, every record found in the table is
    output as a separate XML node. The name of the node is the name of the
    table, and the columns are translated into attributes.
    \begin{lstlisting}[language=XML]
    <Items item_id="1" name="Sprite" price="4.50"/>
    <Items item_id="2" name="Coca-Cola" price="4.25"/>
    <Items item_id="3" name="Diamond Pony" price="9000"/>
    \end{lstlisting}
    
    \item \texttt{PATH}. This option treats every record of the table as a
    separate XML node tree and writes it into an element called \texttt{row}.
    The child nodes of this element correspond to the columns in terms of name
    and value.
    \begin{lstlisting}[language=XML]
    <row>
        <item_id>1</item_id>
        <name>Sprite</name>
        <price>4.50</price>
    </row>
    <row>
        <item_id>2</item_id>
        <name>Coca-Cola</name>
        <price>4.25</price>
    </row>
    <row>
        <item_id>3</item_id>
        <name>Diamond Pony</name>
        <price>9000</price>
    </row>
    \end{lstlisting}
    
    \item \texttt{RAW}. With this option, the user is presented a number of
    options which influence the final document. By default, XML produced with
    this option produces a \texttt{row} element for every row of the database,
    with every non--\texttt{NULL} column mapped to an attribute.
    \begin{lstlisting}[language=XML]
    <row item_id="1" name="Sprite" price="4.50"/>
    <row item_id="2" name="Coca-Cola" price="4.25"/>
    <row item_id="3" name="Diamond Pony" price="9000"/>
    \end{lstlisting}
    
    The \texttt{RAW} option also has a number of sub--options. These include :
    \begin{itemize}
        \item \texttt{ELEMENTS}. Columns are output as elements, and not
        attributes of the output elements.
        
        \item \texttt{XSINIL}. Only available with \texttt{ELEMENTS}. If
        specified, null columns are output in the following way (if
        \texttt{price} was \texttt{NULL}) :	    
	    \begin{lstlisting}[language=XML]
	    <price xsi:nil="true"/>
	    \end{lstlisting}
	    
	    \item \texttt{XMLDATA}. Returns an XML--data schema (Microsoft specific)
	    altogether with the document itself.
	    
	    \item \texttt{XMLSCHEMA}. Returns the XSD schema for the document with
	    the document itself.
	    
	    \item \texttt{BINARY BASE64}. Outputs \texttt{VARBINARY} columns encoded
	    as Base64. Trying to output binary data without this options specified
	    results in an error.
	    
	    \item \texttt{ROOT('NodeName')}. Specifies a root element inside which all
	    the output elements will be contained.
    \end{itemize}
    
    \item \texttt{EXPLICIT}. Allows the user to have the most control over the
    structure of the XML that is being output, at the expense of the most
    complicated queries. When using \texttt{EXPLICIT}, the \texttt{SELECT}
    query used to produce the XML must conform to certain rules in terms of
    column names and values :
    \begin{itemize}
        \item Every query must contain \texttt{Tag} and \texttt{Parent} columns,
        which are used to infer the hierarchy of the XML document :
        \texttt{Tag} is the nesting level of the element, and \texttt{Parent}
        is the \texttt{Tag} of the parent node. If \texttt{Parent} is
        \texttt{NULL}, then the node does not have a parent. Thus, having a
        \texttt{Tag} of 1 and a NULL \texttt{Parent} means that the node is the
        root node.
        
        \item The columns must conform to a special naming scheme. In a
        nutshell, the column name must be \texttt{aaa!bbb!ccc}, where :
        \begin{itemize}
            \item \texttt{aaa} is the name of the node.
            \item \texttt{bbb} is the \texttt{Tag} value of the node.
            \item \texttt{ccc} is the name of the attribute inside this node.
        \end{itemize}
    \end{itemize}
    
    As Microsoft says themselves, \textit{writing EXPLICIT mode queries can be
    cumbersome}.
    \footnote{\url{http://msdn.microsoft.com/en-us/library/ms189068(v=sql.105).aspx}}
\end{itemize}

The Microsoft SQL Server also enables the user to perform XQuery and XPath
queries to XML documents stored inside the database. This is done by the
following functions :
\begin{itemize}
    \item \texttt{query()}. Returns untyped XML as a result of the XQuery
    expression passed to this function.
    \item \texttt{exist()}. Returns 1 if the XQuery expression passed to this
    function returned a non--empty set. Returns 0 otherwise.
    \item \texttt{value()}. This function retrieves a single value from an XML
    document, and performs casting to a standard SQL type, which is the second
    parameter to this function. 
    \item \texttt{modify()}. It modifies the underlying XML data column by
    using the XML DML language, which is somewhat similar to SQL and supports
    \texttt{insert}, \texttt{delete} and \texttt{replace value of} keywords.
\end{itemize}

\section{Non-relational database systems}
In case of non-relational DBs developers didn't have to be bound to the limits
of traditional databases, like need for predefined database schemes.
\begin{description}
    \item[MongoDB] is a document-oriented database system. It requires no schema,
        and is based on JSON document format. While it cannot do the XML operations
        by itself, XML can be converted by application to JSON and then stored
        in MongoDB with minimal additional work (for example, MongoDB allows for
        nested elements just like a regular JSON or XML does)
\end{description}
This was just an example of a non-relational DB that was covered on the laboratories.
There are also DB systems that are created with XML in mind:
\begin{description}
    \item[BaseX] - BSD licenced DB system which supports XQuery and XPath.
        Supports documents formatted as XML, CSV, JSON, HTML, text or binary.
        Provides many APIs, such as RESTful, WebDAV or libraries for many
        programming languages.
    \item[Clusterpoint] - Commercial NoSQL database system, designed for
        distributed document-oriented data stores. Supports XML and JSON.
    \item[eXist] - Open Source (GNU LGPL) database system build entirely on the
        XML technology. Supports XQuery, XPath, SOAP and many other technologies
        like this.
    \item[Sedna] - another native XML database, published under Apache licence.
\end{description}
