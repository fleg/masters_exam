\section{Access control}
Security implemented in RDBMSes can be divided into two separate categories :
\begin{itemize}
    \item Discretionary Access Control (DAC).
    \item Mandatory Access Control (MAC).
\end{itemize}

With DAC, every logged in user has a set of privileges, which fully describe
everything that the user can do while logged in. DAC is directly supported by
SQL by the \texttt{GRANT OPTION} clause. The administrator -- or the user who
has the \texttt{GRANT PRIVILEGE} option -- can grant the privileges to a
certain part of the database (for example, the whole database or a single
table) to another user. Privileges can also often be assigned to user groups,
sometimes called roles, and checking whether a particular user has the
privilege to perform a given operation is done by checking his membership in a
given group.

Assigning privileges with DAC creates a problem when the granting user's
privileges are revoked. Intuitively, the privileges of all the users granted by
this users should be revoked as well. However, this is only supported by
RDBMSes that support the \texttt{CASCADE} clause with the \texttt{GRANT
PRIVILEGE} construct as a non--standard extension.

Furthermore, DAC can incur inconsistencies into the privilege model when
assigning various privileges to a given user. Four resolution models are
supported in order to resolve the inconsistencies. Let us consider the example
when some user is denied the privileges to access a certain database, but is
granted \texttt{SELECT} privileges to one of the tables inside that database.
The resolution models and the effects in this case are :
\begin{itemize}
    \item \textbf{More specific privilege is superior}. The user is allowed to
    execute a \texttt{SELECT} query against the table, but no other table,
    since the \texttt{SELECT} privilege is more specific (e.g. applies to a
    smaller object) than the deny database access privilege.
    \item \textbf{Less specific privilege is superior}. The user is not allowed
    to execute a \texttt{SELECT} query, because the less specific privilege (
    no privileges to access the whole database) is considered more important.
    \item \textbf{Grant privilege is superior}. The user is allowed to execute
    the \texttt{SELECT} query, as it is a grant--type privilege. The ``deny''
    privilege applied to the whole database is less important in this case.
    \item \textbf{Deny privilege is superior}. In this case, the ``deny''
    privilege applied to the whole database is more important and thus the user
    is not allowed to execute the \texttt{SELECT} query.
\end{itemize}

MAC takes a totally different approach to implementing security inside a
database system. Firstly, MAC does not assign privileges to users, but an
access level. Furthermore, these access levels are controlled centrally, by a
database administrator, and nobody else. Thus, it is not possible for normal
users to assign access levels to another users. Finally, every item stored in
the database has an access level as well.

Access levels are defined in terms of security classes. A security class
consists of a security level and the security category. The set of security
levels is finite and ordered, and usually is defined as (in ascending order) :
Unclassified (U), Classified (C), Secret (S) and Top Secret (TS). Security
categories are usually defined in terms of areas of interest of the given
class. Both the security level and the security category are essential in
determining the order of the security classes. When comparing two security
classes, $c_s$ and $c_o$, the following relations are considered :
\begin{itemize}
    \item $c_s \geq c_o \text{ iff } L_s \geq L_o \text{ and } C_o \subseteq C_s$
    \item $c_s \leq c_o \text{ iff } L_s \geq L_o \text{ and } C_s \subseteq C_o$
    \item If neither of these relations hold, the classes are incomparable.
\end{itemize}
Where $L$ is the security level, $C$ is the security category, subscript $s$
means the subject (e.g. the reader/writer) and subscript $o$ -- the object
being accessed.

The rules for accessing objects are also based on the security classes. The
first rule is called the \textit{no read up} rule and states that the subject
can read only the objects which have a lower or equal security class. The
second rule, called the \textit{star property}, says that subjects can only
write to objects whose security class is considered higher than the subject's
security class. Furthermore, it is also possible to distinguish between the
current security class and the maximum security class : a subject with a higher
security class can choose to log in to the system with a lower security class
in order to write documents with this lower security class.

\section{Encryption}
There are three levels of encryption distinguishable in RDBMSes :
\begin{itemize}
    \item File system encryption.
    \item Database--level encryption.
    \item Cell--level encryption.
\end{itemize}

File system encryption refers to the solution available by most of the today's
operating systems. When using it, the content stored to the physical drive is
actually encrypted, but that encryption is managed entirely by the operating
system and transparent to all the running applications. The key needed to
decrypt the data is either entered manually or retrieved from a storage media.
This method has a number of drawbacks when considering its usage altogether
with an RDBMS : for example, it does not allow to separate the roles of the
system administrator and database administrator. Furthermore, this kind of
security might be considered insufficient for some uses due to the way how it's
implemented : the contents of the encrypted drive are usually visible to all
the users of the system, unencrypted data is consistently stored in the memory,
and may leak during hibernation.

Database--level encryption is somewhat similar to file system encryption, but
it's implemented entirely by the RDBMS : with it, the whole database is
encrypted before being written to the disk. The encryption is transparent to
the clients using the database, and all the data appears as unencrypted. This
kind of encryption is configured by the database administrator.

Cell--level encryption enables the user of the database to encrypt a single
cell of data. However, the encrypted data is stored inside the database as the
\texttt{VARBINARY} or \texttt{BLOB} type, as most RDBMSes do not have a
separate column type for storing encrypted data. Furthermore, the commands
issued to the database must contain proper function calls that decrypt the data
actually contained inside the database into actual, usable, unencrypted data.
RDBMSes greatly vary in terms of decrypting cell--level encrypted data. For
example, MsSQL requires the user to firstly create a key via \texttt{CREATE
(A)SYMMETRIC KEY}, which is then referenced in function calls responsible for
encryption and decryption. MySQL, on the other hand, requires the user to
directly input the key as a parameter to the encryption/decryption function.
Cell--level encryption can have a big impact on the database's performance not
only because of the encryption itself, but mainly because of the fact that most
RDBMSes do not allow indexing of \texttt{VARBINARY} or \texttt{BLOB} columns
(assuming that they used to be indexed, when non--encrypted).
