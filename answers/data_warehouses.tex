The prime purpose of a Data Warehouse is to store, in one system, data and
information that originates from multiple applications within, or across,
organisations. The data may be stored 'as received' from the source application,
or it may be processed upon input to validate, translate, aggregate or derive
new data/information. System have to ensure high quality of analytical data and
that there will be only single version of truth.

A data warehouse is created in the \textbf{ETL} process, which is shorthand for
\textit{Extract, Transform and Load}. The main actions taken by this process
include :
\begin{itemize}
    \item Extracting the data from the data sources. This may include a number
    of sources, which should nevertheless be related to one another. The source
    of the data itself is irrelevant : it may come from an SQL database, from
    flat files (CSV, binary, or other), or from network resources.
    \item Transforming the data in order to maintain integrity between various
    data sources. This includes changing the encoding of certain fields to be
    consistent inside the data warehouse itself, resolving null values to
    values that are recognized inside the warehouse, and resolving foreign key
    relationships.
    \item Loading the data into the warehouse. Once the data is loaded from the
    data sources and properly transformed, it is put into the warehouse, as
    it's ready for performing actual analysis.
\end{itemize}

Data warehouse is subject oriented, integrated, non-volatile and time-varying
collection of data. Subject oriented means that data are organized around
subject of interest to data analyst, for example customer, product or supplier.
Integrated means that data warehouse integrates data from several data sources
and ensure that all attributes are coded in a consistent way. Non-volatile
means that data loaded into data warehouse is a snapshot of operational data at
a specific point in time and once loaded cannot be changed. Time-varying means
that data elements in warehouse are timestamped to facilitate analysis of
changes or trends over time.

The architecture of a data warehouse is based on OLAP, which is shorthand for
OnLine Analytical Processing. OLAP assumes a multi--dimensional model of the
data inside the warehouse : the data is modelled as a cube, which contains
several measures (objects of analysis, facts), dependent on the values of the
dimensions of the cube. OLAP itself can be implemented in a number of ways :
\begin{itemize}
    \item ROLAP (Relational OLAP). It uses an ordinary relational DBMS as the
    data storage backend. The schema of such a relational database usually
    follows the star schema, in which there is a single fact table, and a
    single table for each dimension of the cube. The snowflake schema, which is
    an extension of the star schema, allows the dimension tables to have
    foreign key relationships to other tables.
    \item MOLAP (Multidimensional OLAP). With MOLAP, the data is stored inside
    a specialized data store, which directly reflects the multi--dimensional
    model of the data. In order to make the performance of the queries higher,
    the data is pre--summarized. Some of the most often used facts and
    dimensions can also be stored in sub--cubes for higher performance.
    \item HOLAP (Hybrid OLAP). HOLAP assumes the existence of a
    multidimensional data provider (MDP), which serves as a proxy between the
    client (the data analysis service) and the storage backend. This proxy can
    dispatch the requests to different kinds of actual storages : MOLAP-- or
    ROLAP-- based. Two techniques can be distinguished : racking, which uses
    an individual DB for every dimension, and stacking, which stores separate
    subcubes in separate databases or tables.
\end{itemize}
